﻿/* Program ID: WwangAssignment3
 * Assignment 3
 * 
 * Revision History
 * Created by Weichen Wang Nov 2018
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WwangAssignment3
{


    /// <summary>
    /// A class that creates a game of tic-tac-toe for two-person
    /// playing
    /// </summary>
    /// 
    public partial class Form1 : Form
    {

        private PictureBox[,] tiles;
        private bool playerX = true;

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Dynamically generates the tic-tac-toe playing field on
        /// form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void Form1_Load(object sender, EventArgs e)
        {
            int x = 125;
            int y = 100;
            int LEFT = 125;
            int WIDTH = 75;
            int HEIGHT = 75;
            int HGAP = 1;
            int VGAP = 1;
            int rowLength = 3;
            tiles = new PictureBox[rowLength, rowLength];

            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    PictureBox tile = new PictureBox();
                    tiles[i, j] = tile;

                    tile.Left = x;
                    tile.Top = y;
                    tile.Size = new Size(WIDTH, HEIGHT);
                    tile.BorderStyle = BorderStyle.FixedSingle;

                    this.Controls.Add(tile);
                    tile.Click += PictureBox_Click;

                    x += WIDTH + HGAP;
                }
                y += HEIGHT + VGAP;
                x = LEFT;
            }
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            PictureBox tile = (PictureBox)sender;

            if (tile.Image == null)
            {
                if (playerX == true)
                {
                    tile.Image =
                        WwangAssignment3.Properties.Resources.x;
                    tile.Tag = "X";
                    playerX = false;
                }
                else if (playerX == false)
                {
                    tile.Image = WwangAssignment3.Properties
                        .Resources.o;
                    tile.Tag = "O";
                    playerX = true;
                }
                checkGameStatus(tiles);
            }
        }

        /// <summary>
        /// Method used after each turn to check if game is
        /// completed or needs to continue
        /// </summary>
        /// <param name="tiles">Array containing all
        /// game pictureboxes</param>

        private void checkGameStatus(PictureBox[,] tiles)
        {
            if (Convert.ToString(tiles[0, 0].Tag) == "X" &&
                Convert.ToString(tiles[1, 0].Tag) == "X" &&
                Convert.ToString(tiles[2, 0].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();

            }
            else if (Convert.ToString(tiles[0, 1].Tag) == "X" &&
                Convert.ToString(tiles[1, 1].Tag) == "X" &&
                Convert.ToString(tiles[2, 1].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 2].Tag) == "X" &&
                Convert.ToString(tiles[1, 2].Tag) == "X" &&
                Convert.ToString(tiles[2, 2].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 0].Tag) == "X" &&
                Convert.ToString(tiles[0, 1].Tag) == "X" &&
                Convert.ToString(tiles[0, 2].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[1, 0].Tag) == "X" &&
                Convert.ToString(tiles[1, 1].Tag) == "X" &&
                Convert.ToString(tiles[1, 2].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[2, 0].Tag) == "X" &&
                Convert.ToString(tiles[2, 1].Tag) == "X" &&
                Convert.ToString(tiles[2, 2].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 0].Tag) == "X" &&
                Convert.ToString(tiles[1, 1].Tag) == "X" &&
                Convert.ToString(tiles[2, 2].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 2].Tag) == "X" &&
                Convert.ToString(tiles[1, 1].Tag) == "X" &&
                Convert.ToString(tiles[2, 0].Tag) == "X")
            {
                MessageBox.Show("X wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 0].Tag) == "O" &&
                Convert.ToString(tiles[1, 0].Tag) == "O" &&
                Convert.ToString(tiles[2, 0].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 1].Tag) == "O" &&
                Convert.ToString(tiles[1, 1].Tag) == "O" &&
                Convert.ToString(tiles[2, 1].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 2].Tag) == "O" &&
                Convert.ToString(tiles[1, 2].Tag) == "O" &&
                Convert.ToString(tiles[2, 2].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 0].Tag) == "O" &&
                Convert.ToString(tiles[0, 1].Tag) == "O" &&
                Convert.ToString(tiles[0, 2].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[1, 0].Tag) == "O" &&
                Convert.ToString(tiles[1, 1].Tag) == "O" &&
                Convert.ToString(tiles[1, 2].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[2, 0].Tag) == "O" &&
                Convert.ToString(tiles[2, 1].Tag) == "O" &&
                Convert.ToString(tiles[2, 2].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 0].Tag) == "O" &&
                Convert.ToString(tiles[1, 1].Tag) == "O" &&
                Convert.ToString(tiles[2, 2].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else if (Convert.ToString(tiles[0, 2].Tag) == "O" &&
                Convert.ToString(tiles[1, 1].Tag) == "O" &&
                Convert.ToString(tiles[2, 0].Tag) == "O")
            {
                MessageBox.Show("O wins!");
                rematch();
            }
            else
            {
                int counter = 0;

                for (int i = 0; i < tiles.GetLength(0); i++)
                {
                    for (int j = 0; j < tiles.GetLength(1); j++)
                    {
                        if (tiles[i, j].Image != null)
                        {
                            counter++;
                        }
                    }
                }

                if (counter == tiles.Length)
                {
                    MessageBox.Show("Tie");
                    rematch();
                }
            }
        }
        /// <summary>
        /// Re-initializes the form to play another game and
        /// hide the finished game
        /// </summary>
        private void rematch()
        {
            Form1 rematch = new Form1();
            rematch.Show();
            this.Hide();
        }
    }
}
